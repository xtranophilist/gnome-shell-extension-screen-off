gnome-shell-extension-screen-off
================================

This is an extension that adds a button to gnome panel that when clicked instantly turns off the monitor.
'xset dpms force off' is executed in the backend.

**[Note: This extension isn't maintained any more.]**

Homepage:
[http://motorscript.com/gnome-shell-extension-screen-turn-off-button/](http://motorscript.com/gnome-shell-extension-screen-turn-off-button/ "http://motorscript.com/gnome-shell-extension-screen-turn-off-button/")

**Installation Instructions:**

Download:  
`cd ~/.local/share/gnome-shell/extensions`  
`git clone https://bitbucket.org/xtranophilist/gnome-shell-extension-screen-off.git`  
`mv gnome-shell-extension-screen-off/ screen-off@xtranophilist`  

Restart Gnome:
Alt+F2 to open run command prompt
Enter r

Enable 'Screen Off Extension' from Extensions from `gnome-tweak-tool`